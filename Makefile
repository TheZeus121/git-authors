SHELL = /bin/sh
.SUFFIXES:

NAME := "git-authors"
VERSION := "0.1.0"

INSTALL ?= $(shell which install)

INSTALL_PROGRAM ?= $(INSTALL)
INSTALL_DATA ?= $(INSTALL) -m 644

srcdir ?= .

prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
datarootdir ?= $(prefix)/share
licensedir ?= $(datarootdir)/licenses
mandir ?= $(datarootdir)/man
man1dir ?= $(mandir)/man1
manext ?= ".1"

all:
	@echo "Nothing to do..."
	@echo "You can run make install"

install: install-bin install-doc install-license

install-doc: installdirs
	@echo "Installing documentation"
	@$(INSTALL_DATA) git-authors.1 $(DESTDIR)$(man1dir)/git-authors$(manext)

install-bin: installdirs
	@echo "Installing executable"
	@$(INSTALL) git-authors $(DESTDIR)$(bindir)/git-authors

install-license: installdirs
	@echo "Installing license"
	@$(INSTALL_DATA) LICENSE $(DESTDIR)$(licensedir)/git-authors/LICENSE

installdirs:
	@$(srcdir)/mkinstalldirs $(DESTDIR)$(bindir) $(DESTDIR)$(man1dir)\
                           $(DESTDIR)$(licensedir)/git-authors

uninstall: uninstall-bin uninstall-doc uninstall-license

uninstall-bin:
	@echo "Uninstalling executable"
	@rm $(DESTDIR)$(bindir)/git-authors

uninstall-doc:
	@echo "Uninstalling documentation"
	@rm $(DESTDIR)$(man1dir)/git-authors$(manext)

uninstall-license:
	@echo "Uninstall license"
	@rm $(DESTDIR)$(licensedir)/git-authors/LICENSE

clean:

love:
	@echo "Not war"

dist:
	@mkdir "$(NAME)-$(VERSION)"
	@ln -s ../AUTHORS ../CHANGELOG.md ../LICENSE ../Makefile ../README.md \
         ../git-authors ../git-authors.1 ../mkinstalldirs \
         "$(NAME)-$(VERSION)"
	@tar -czhf "$(NAME)-$(VERSION).tar.gz" "$(NAME)-$(VERSION)"
	@rm -rf "$(NAME)-$(VERSION)"
